#FROM alpine:latest
FROM cypress/browsers:node12.13.0-chrome80-ff74

MAINTAINER Leo Cheron <leo@cheron.works>

RUN npm i -g @lhci/cli@0.3.x